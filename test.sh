#!/bin/sh

RES="$(cat fruits.pug | docker run -i registry.gitlab.com/rosaenlg-projects/docker-cli-rosaenlg:master -l en_US)"
EXPECTED="<p>I love apples, bananas, apricots and pears!</p>"

echo "$RES"

if test "$RES" = "$EXPECTED"; then
  echo "TEST: OK!"
  exit 0
else
  echo "TEST: FAILS! :-("
  exit 1
fi
