# Docker Command Line Interface for RosaeNLG

Docker image for RosaeNLG that contains essentially [RosaeNLG's CLI](https://gitlab.com/rosaenlg-projects/rosaenlg-cli).

## Documentation

For documentation, see:
- [RosaeNLG documentation](https://rosaenlg.org)
- and [here](doc/modules/docker-cli/pages/docker-cli.adoc)
